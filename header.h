#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
typedef  struct{
  long long num;
  long long denom;
}Fraction;
#ifdef INT
typedef int value_type;
typedef double key_type;
#endif
#ifdef CHAR
typedef char value_type [80];
typedef int key_type;
#endif
#ifdef FRACT
typedef Fraction *value_type;
typedef double key_type;
#endif


typedef struct NODE {
    value_type value;
    key_type key;
    struct NODE* next;
    struct NODE* prev;
      struct NODE* prev_sorted;
      struct NODE *sort;
} Node;
typedef struct LIST{
  int size;
  struct NODE *head;
  struct NODE *sort_head;
  struct NODE *tail;
  struct NODE *sort_tail;
}Sorted_List;
#define FALSE 0
#define TRUE 1
#define SIMPLE 0
#define MIXED 1
/*Misc*/
int size(Sorted_List *list);
Node * create_node();
/*printing*/
void print_sort(Sorted_List *list);
void print_all(Sorted_List *list);
/*moving list*/
Node *sorting(Sorted_List *list);
int push(Sorted_List *list, value_type value, key_type key );
int append(Sorted_List *list, value_type value, key_type key );
void prev_sort(Sorted_List *list);
void prev(Sorted_List *list);
/*removing list*/
int remove_smallest_key(Sorted_List *list, value_type* value, key_type *key);
int remove_first(Sorted_List *list,value_type *value,key_type *key);
int remove_largest_key(Sorted_List *list, value_type* value, key_type *key);
int remove_largest_key(Sorted_List *list, value_type* value, key_type *key);
int remove_last(Sorted_List *list,value_type *value ,key_type *key);
/*freeing list*/
void empty_list(Sorted_List *list);
void destroy_list(Sorted_List *list);
/*map stuff*/
#ifdef INT
Sorted_List *map(Sorted_List *list, value_type (*fn_ptr)(value_type,value_type));
value_type exp2 (value_type value,value_type exponent);
value_type reduce (Sorted_List *list, value_type(*reduce)(value_type,value_type),value_type init,int order);
value_type mult(value_type x, value_type y);
value_type add(value_type x, value_type y);
value_type *map_2_array(Sorted_List *list,Sorted_List *list2, value_type(*map_2_array)(value_type,value_type),int order);
value_type map_reduce(Sorted_List *list, value_type (*map)(value_type,value_type),value_type (*reduce)(value_type, value_type),value_type init, int order);
value_type map_2_reduce (Sorted_List *list1,Sorted_List *list2,value_type (*map)(value_type,value_type),value_type(*reduce)(value_type,value_type),int order);
int run(char *command, key_type dinput,value_type iinput,Sorted_List *list1,Sorted_List *list2, char *command2,int listone,int listtwo,int order);
value_type subtract(value_type x,value_type y);
#endif
/*recursion*/
void count_up(int number,int count);
void count_down(int count);
void nth(Sorted_List *list ,Node *node,int count,int number,int order);
void remove_nth(Sorted_List *list ,Node *node,int count,int number,int order);
/*fractions*/

void print_fract (Fraction *fract,int mode);
int set_fraction(Fraction *fract,long long num,long long denom);
void simplify (Fraction *fract);
long long gcd(long long x, long long y);
int add_fract(Fraction *result,Fraction *x, Fraction *y);
Sorted_List *filter(Sorted_List *list, int(filter_ptr(Node*)));
void print_all_fract(Sorted_List *list, int mode);
void print_sort_fract(Sorted_List *list, int mode);
int if_fract(Node *node);
int if_whole(Node *node);
int if_mixed(Node *node);
