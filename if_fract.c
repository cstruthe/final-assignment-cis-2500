#include "header.h"

int if_fract(Node *node){
  Fraction * temp=node->value;
  /*if the remainder is not zero it's a fraction therefore returns true*/
  if(temp->num%temp->denom!=0){
    return TRUE;
  }else{
    return FALSE;
  }
}
