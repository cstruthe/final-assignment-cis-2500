#include "header.h"

void print_all(Sorted_List *list){
  Node *node=list->head;
  while(node!=NULL){
    /*for int*/
    #ifdef INT
    printf("     %.2f %d \n",node->key,node->value );
    #endif
    /*for char*/
    #ifdef CHAR
    printf("     %d   %s",node->key,node->value );
    #endif
    node=node->next;

  }
}
