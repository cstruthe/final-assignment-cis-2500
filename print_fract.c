#include "header.h"

void print_fract (Fraction *fract,int mode){
  Fraction printing;
  printing.num=fract->num;
  printing.denom=fract->denom;
  long long temp;
  long long mixed=0;
  /*find gcd*/
  long long division=gcd(printing.num,printing.denom);
  printing.num=printing.num/division;
  printing.denom=printing.denom/division;
  fract->num=fract->num/division;
  fract->denom=fract->denom/division;
  if(mode==SIMPLE){
    /*for simple*/
    printf("%lld/%lld\n",printing.num,printing.denom );
  }else if(mode==MIXED){
    /*if mixed and num is smaller than denom*/
    if(printing.num<=printing.denom&&printing.denom!=1){
      printf("%lld/%lld\n",printing.num,printing.denom );
    }else if(printing.num>printing.denom){
      /*if mixed and num is larger than denom*/
      temp=printing.denom;
      if(printing.num%printing.denom!=0){
        while(printing.num>printing.denom){
          printing.num=printing.num-temp;
          mixed++;
        }
        printf("%lld %lld/%lld\n",mixed,printing.num,printing.denom );
      }else{
        temp=printing.num/printing.denom;
        printf("%lld \n",temp );
      }
    }else if(printing.denom==1){
      /*if the denom is 1*/
      printf("%lld\n" ,printing.num );
    }
  }
}
