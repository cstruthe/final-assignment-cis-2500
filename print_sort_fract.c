#include "header.h"

void print_sort_fract(Sorted_List *list, int mode){
  Node*node=list->sort_tail;
  /*prints the list in sorted order(sorted using key)*/
  while(node!=NULL){
    printf("%f  ",node->key );

    print_fract(node->value,mode);
    node=node->prev_sorted;
  }
}
