#include "header.h"
void destroy_list(Sorted_List *list){
  Node* node=list->head;
  /*frees the list node by node*/
  while(node!=NULL){

      #ifdef FRACT
      free(node->value);
      #endif
      free(node);
      node=node->next;
    }
  /*frees node*/
  free(list);
}
