#include "header.h"

value_type map_2_reduce (Sorted_List *list1,Sorted_List *list2,value_type (*map)(value_type,value_type),value_type(*reduce)(value_type,value_type),int order){
  value_type results=0;
  value_type result2=0;
  Node *node=NULL;
  Node *node2=NULL;
  int i=0;
  /*check list size*/
  if(list1->size!=list2->size){
    printf("error list are not the same size\n");
    return 0;
  }
  /*for Insertion Order*/
  if(order==0){
    node=list1->head;
    node2=list2->head;
    for(i=0; i<list1->size; i++){
      /*runs the two function pointers*/
      results=reduce(node2->value,node->value);
      result2=result2+map(results,2);
      node=node->next;
      node2=node2->next;
    }
    /*for sorted order*/
  }else if(order==1){
    node=list1->sort_tail;
    node2=list2->sort_tail;
    for(i=0; i<list1->size; i++){
      /*runs the two function pointers*/
      results=reduce(node2->value,node->value);
      result2=result2+map(results,2);
      node=node->prev_sorted;
      node2=node2->prev_sorted;
    }
  }
  return result2;
}
