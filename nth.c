#include "header.h"

void nth(Sorted_List *list ,Node *node,int count,int number,int order){

  if(order==0){
    if(count==number&&number!=0){
      /*INSERTED_ORDER*/
      node=list->head;
      count--;
      if(node->next!=NULL){
        nth(list,node->next,count,number,order);
      }else {
        printf("nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
      }
    }else if(count==0&&number!=0){
        printf("nth:    n=%d, INSERTED_ORDER\n",number);
        printf("     %.2f %d\n",node->key, node->value );
    }else if(count==0&&number==0){

      node=list->head;
      printf("nth:    n=%d, INSERTED_ORDER\n",number);
      printf("     %.2f %d\n",node->key, node->value );
    }
    else{
      if(node->next!=NULL){
        count--;
        nth(list,node->next,count,number,order);
      }else {
        printf("nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
      }
    }
  }else if(order==1){
        /*SORTED_ORDER*/
    if(count==number&&number!=0){
      node=list->sort_tail;
      count--;
      if(node->prev_sorted!=NULL){
        nth(list,node->prev_sorted,count,number,order);
      }else {
        printf("nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
      }
    }else if(count==0&&number!=0){
        printf("nth:    n=%d, SORTED_ORDER\n",number);
        printf("     %.2f %d\n",node->key, node->value );
    }else if(number==0)
    {
        node=list->sort_tail;
        printf("nth:    n=%d, SORTED_ORDER\n",number);
        printf("     %.2f %d\n",node->key, node->value );
    }else{
      if(node->prev_sorted!=NULL){
        count--;
        nth(list,node->prev_sorted,count,number,order);
      }else {
        printf("nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
      }
    }
  }
}
