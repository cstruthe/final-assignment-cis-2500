#include "header.h"

int size(Sorted_List *list){
  int i=0;
  Node *next_node=list->head;
  /*for each node adds one to i*/
  while(next_node!=NULL){
    i++;
    next_node=next_node->next;
  }
  list->size=i;
  return i;
}
