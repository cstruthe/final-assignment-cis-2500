#include "header.h"

int main(int argc, char *argv[]){
  int i=0;
  int mode=0;
  FILE *fp;
  int end=0;
  Node *node;
  Node *next;
  Fraction *temp2=malloc(sizeof(Fraction));
  char *command=malloc(sizeof(char)*80);
  double dinput=0;
  char *input=malloc(sizeof(char)*80);
  char *char_fraction=malloc(sizeof(char)*80);
  char *char_num=malloc(sizeof(char)*80);
  char *char_denom=malloc(sizeof(char)*80);
  Sorted_List *list=malloc(sizeof(Sorted_List));
  Sorted_List *whole=NULL;
  Sorted_List *fract=NULL;
  Sorted_List *non_mixed=NULL;
  long long denom=0;
  long long num=0;
  list->size=0;
  int result=0;
  int loop=0;
  Fraction *temp=malloc(sizeof(Fraction));
  /*checks argv*/
  if(argv[1]!=NULL&&strcmp(argv[1],">")!=0){
  fp=fopen(argv[1], "r");
}
    /*if not*/
  if(fp==NULL){
    printf("please enter a file name\n");
    do{
        fgets(input,200,stdin);
        if(feof(stdin)!=0){
          loop=1;
        }
      }while(loop==0);
    fp=fopen(input,"r");
  }
  /*if file is not found*/
  if(fp==NULL){
    printf("error file not found\n");
    return 0;
  }
      while(fscanf(fp,"%s %s",command,char_fraction)!=EOF){
        if(strcmp(command,"p")==0){
          /*push*/
          i=0;
          /*gets the num and denom from the char statement*/
          while(char_fraction[i]!='/'&&i<strlen(char_fraction)){
            char_num[i]=char_fraction[i];
            i++;
          }
          num=atol(char_num);
          i++;
          end=i;
          for(i=i; i<=strlen(char_fraction); i++){
            char_denom[i-end]=char_fraction[i];
          }

          if(strlen(char_denom)>0){
            denom=atol(char_denom);
            set_fraction(temp,num,denom);
            dinput=(double)temp->num/temp->denom;
            printf("%s:         %f  %s \n",command,dinput,char_fraction );
          }else{
            denom=1;
            set_fraction(temp,num,denom);
            dinput=(double)temp->num;
            printf("%s:         %f  %lld\n",command,dinput,temp->num );
          }
          result=push(list,temp,dinput);
          if(result==0){
            return 0;
          }
        }else if(strcmp(command,"a")==0){
          /*append*/
          i=0;
          /*gets the num and denom from the char statement*/
          while(char_fraction[i]!='/'&&i<strlen(char_fraction)){
            char_num[i]=char_fraction[i];
            i++;
          }
          num=atol(char_num);
          i++;
          end=i;
          for(i=i; i<=strlen(char_fraction); i++){
            char_denom[i-end]=char_fraction[i];
          }

          if(strlen(char_denom)>0){
            denom=atol(char_denom);
            set_fraction(temp,num,denom);
            dinput=(double)temp->num/temp->denom;
            printf("%s:         %f  %s \n",command,dinput,char_fraction );
          }else{
            denom=1;
            set_fraction(temp,num,denom);
            dinput=(double)temp->num;
            printf("%s:         %f  %lld\n",command,dinput,temp->num );
          }

          result=append(list,temp,dinput);
          if(result==0){
            return 0;
          }
        }else if(strcmp(command,"print_all")==0){
          /*print all*/
          if(strcmp(char_fraction,"SIMPLE")==0){
            mode=0;
          }else if(strcmp(char_fraction,"MIXED")==0){
            mode=1;
          }
          printf("%s:  ",command );
          if(mode==0){
            printf("Simple Fractions, Insertion Order\n");
          }else if(mode==1){
            printf("Mixed Fractions, Insertion Order\n");
          }
          print_all_fract(list,mode);
        }else if(strcmp(command,"print_sort")==0){
          if(strcmp(char_fraction,"SIMPLE")==0){
            mode=0;
          }else if(strcmp(char_fraction,"MIXED")==0){
            mode=1;
          }
          printf("%s:  ",command );
          if(mode==0){
            printf("Simple Fractions, Sorted Order\n");
          }else if(mode==1){
            printf("Mixed Fractions, Sorted Order\n");
          }
          print_sort_fract(list,mode);
        }else if(strcmp(command,"sum")==0){
          if(strcmp(char_fraction,"SIMPLE")==0){
            mode=0;
          }else if(strcmp(char_fraction,"MIXED")==0){
            mode=1;
          }
          node=list->head;
          next=node->next;
          temp->denom=0;
          temp->num=0;
          i=add_fract(temp,node->value,next->value);
          temp2->num=temp->num;
          temp2->denom=temp->denom;
          node=next->next;
          while(node!=NULL&&i!=FALSE){
            i=add_fract(temp,temp2,node->value);
            temp2->num=temp->num;
            temp2->denom=temp->denom;
            temp->num=0;
            temp->denom=0;
            node=node->next;
          }

          printf("%s:          results=",command );
              if(i!=FALSE){
                print_fract(temp2,mode);
              }else{
                printf("OVERFLOW\n");
              }
        }else if(strcmp(command,"fract")==0){
          /*print all fractions*/
          if(fract!=NULL){
            destroy_list(fract);
          }
          fract=filter(list,if_fract);
          if(strcmp(char_fraction,"SIMPLE")==0){
            mode=0;
            printf("%s:     Simple Fraction, Insertion Order\n",command);
          }else if(strcmp(char_fraction,"MIXED")==0){
            mode=1;
            printf("%s:     Mixed Fraction, Insertion Order\n",command);
          }
          print_all_fract(fract,mode);
        }else if(strcmp(command,"whole_num")==0){
          /*prints all whole nums*/
          if(whole!=NULL){
            destroy_list(whole);
          }
          whole=filter(list,if_whole);
          if(strcmp(char_fraction,"SIMPLE")==0){
            mode=0;
            printf("%s:     Simple Fraction, Insertion Order\n",command);
          }else if(strcmp(char_fraction,"MIXED")==0){
            mode=1;
            printf("%s:     Mixed Fraction, Insertion Order\n",command);
          }
          print_all_fract(whole,mode);
          destroy_list(whole);
          whole=NULL;
        }else if(strcmp(command,"rem_mixed")==0){
          /*print all non mixed things*/
          if(non_mixed!=NULL){
            destroy_list(non_mixed);
          }
          non_mixed=filter(list,if_mixed);
          whole=filter(list,if_whole);
          if(strcmp(char_fraction,"SIMPLE")==0){
            mode=0;
            printf("%s:     Simple Fraction, Insertion Order\n",command);
          }else if(strcmp(char_fraction,"MIXED")==0){
            mode=1;
            printf("%s:     Mixed Fraction, Insertion Order\n",command);
          }
          print_all_fract(non_mixed,mode);
        }
        memset(char_denom,0,80);
        memset(char_num,0,80);
      }
      destroy_list(list);
      if(whole!=NULL){
        destroy_list(whole);
      }
      if(fract!=NULL){
        destroy_list(fract);
      }
      if(non_mixed!=NULL){
        destroy_list(non_mixed);
      }
      free(input);
      free(char_denom);
      free(char_fraction);
      free(char_num);
      free(command);
      free(temp);
      free(temp2);
      fclose(fp);
  return 1;
}
