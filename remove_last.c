#include "header.h"

int remove_last(Sorted_List *list,value_type *value ,key_type *key){
  int result=1;
  Node *temp;
  Node *temp2;
  /*checks if list is made if not ends program*/
  if(list->tail!=NULL){

  temp=list->tail;
  }else{
  return 0;
  }
  /*sets a new tail*/
  if(temp->prev!=NULL){
    temp2=temp->prev;
  }else{
    return 0;
  }
  /*copies the values*/
  list->tail=temp->prev;
  temp2->next=NULL;
  #ifdef INT
  *value=temp->value;
  *key=temp->key;
  #endif
  #ifdef CHAR
  strcpy(*value,temp->value);
  *key=temp->key;
  #endif
  /*checks if if the last key is the largest or smallest node*/
  if(list->sort_head==temp){
    if(temp->sort!=NULL){
      list->sort_head=temp->sort;
    }
  }
  if(list->sort_tail==temp){
    if(temp->prev_sorted!=NULL){
      list->sort_tail=temp->prev_sorted;
    }
  }
  free(temp);
  /*resorts sort and prevs */
  sorting(list);
  prev(list);
  prev_sort(list);
  list->size--;
  if(list->tail==NULL){
    return 0;
  }

  return result;
}
