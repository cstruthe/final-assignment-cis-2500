#include "header.h"


int add_fract(Fraction *result,Fraction *x, Fraction *y){
  long long leftside=0;
  long long rightside=0;
  long long total=0;
  long long denom=0;
  long long division=gcd(y->num, y->denom);
  y->num=y->num/division;
  y->denom=y->denom/division;
  if(x->num==-1&&y->denom==LONG_MAX){
    return FALSE;
  }
  if(y->num==-1&&x->denom==LONG_MAX){
    return FALSE;
  }
  if(x->num>LONG_MAX/y->denom){
    return FALSE;
  }
  if(y->num>LONG_MAX/x->denom){
    return FALSE;
  }
  leftside=x->num*y->denom;
  rightside=y->num*x->denom;
  if(leftside>0&&rightside>LONG_MAX-leftside){
    return FALSE;
  }
  if(leftside<0&&rightside<LONG_MIN-leftside){
    return FALSE;
  }
  total=leftside+rightside;
  if(x->denom==-1&&y->denom==LONG_MAX){
    return FALSE;
  }
  if(x->denom>LONG_MAX/y->denom){
    return FALSE;
  }
  denom=x->denom*y->denom;
  result->num=total;
  result->denom=denom;
  return TRUE;
}
