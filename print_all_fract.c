#include "header.h"

void print_all_fract(Sorted_List *list, int mode){
  Node *node=list->head;
  /*goes through the list and prints the value*/
  while(node!=NULL){
    printf("%f  ",node->key );

    print_fract(node->value,mode);
    node=node->next;
  }
}
