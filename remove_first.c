#include "header.h"

int remove_first(Sorted_List *list,value_type *value,key_type *key){
  int result=1;
  Node *temp;
  /*checks if list was made*/
  if(list->head!=NULL){
    temp=list->head;
  }else{
    return 0;
  }
  /*copies values*/
  #ifdef INT
  *value=temp->value;
  *key=temp->key;
  #endif

  #ifdef CHAR
  strcpy(*value,temp->value);
  *key=temp->key;
  #endif
  /*reassigns head*/
  if(temp->next!=NULL){
  list->head=temp->next;
  }else{
    return 0;
  }
  /*resorts list*/
  sorting(list);
  if(list->sort_head==temp){
    if(temp->sort!=NULL){
      list->sort_head=temp->sort;
    }
  }
  if(list->sort_tail==temp){
    if(temp->prev_sorted!=NULL){
      list->sort_tail=temp->prev_sorted;
    }
  }
  free(temp);
  if(list->head==NULL){
    return 0;
  }
  /*resorts prevs*/
  prev(list);
  prev_sort(list);
  list->size--;
  return result;
}
