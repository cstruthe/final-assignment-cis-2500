#include "header.h"

int remove_smallest_key(Sorted_List *list, value_type* value, key_type *key){
  int result=1;
  Node *temp;
  Node *temp2;
  Node *temp3;
  /*sets temp to smallest key*/
  if(list->sort_tail!=NULL){
    temp=list->sort_tail;
  }else{
    return 0;
  }
  /*gets value from smallest key*/
  #ifdef INT
  *key=temp->key;
  *value=temp->value;
  #endif
  #ifdef CHAR
  *key=temp->key;
  strcpy(*value,temp->value);
  #endif
  /*sets the tail to be the second smallest value*/
  if(temp->prev_sorted!=NULL){
  list->sort_tail=temp->prev_sorted;
  }else{
  return 0;
  }
  temp2=temp->prev_sorted;
  temp2->sort=NULL;

  if(temp==NULL){
    return 0;
  }
  /*checks the original list to reconfigure it*/
  if(temp==list->tail){
    list->tail=temp->prev;
  }
  temp2=list->head;
  while(temp!=temp2&&temp2!=NULL){
    temp3=temp2;
    temp2=temp2->next;
  }
  if(temp==list->head){
    list->head=temp->next;
  }else{
    if(temp2->next!=NULL){
      temp3->next=temp2->next;
    }else{
      temp3->next=NULL;
      list->tail=temp3;
    }
   }
   /*frees the final point and redoes the prev and prev_sort*/
   free(temp);
   temp=NULL;
   list->size--;
   prev(list);
   prev_sort(list);
   return result;
}
