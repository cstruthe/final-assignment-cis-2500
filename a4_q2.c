#include "header.h"

int main(int argc ,char *argv[]){
  FILE *fp;
  value_type *value=malloc(sizeof(value_type));
  char *command=malloc(sizeof(char)*80);
  double dinput=0;
  char *input=malloc(sizeof(char)*80);
  int iinput=0;
  key_type *key=malloc(sizeof(key_type));
  Sorted_List *list=malloc(sizeof(Sorted_List));
  list->size=0;
  int result=0;
  int loop=0;
  /*checks argv*/
  if(argv[1]!=NULL){
  fp=fopen(argv[1], "r");
  }
    /*if not*/
  if(fp==NULL){
    printf("please enter a file name\n");
    do{
        fgets(input,200,stdin);
        if(feof(stdin)!=0){
          loop=1;
        }
      }while(loop==0);
    fp=fopen(input,"r");
  }
  /*if file is not found*/
  if(fp==NULL){
    printf("error file not found\n");
    return 0;
  }


    while(fscanf(fp,"%s %lf %d",command,&dinput,&iinput)!=EOF){
      /*push*/
      if(strcmp(command,"p")==0){
        printf("%s:         %.2f  %d\n",command,dinput,iinput );
        result=push(list,iinput,dinput);
        if(result==0){
          return 0;
        }
      }else if(strcmp(command,"a")==0){
        /*append*/
        printf("%s:         %.2f  %d\n",command,dinput,iinput );
        result=append(list,iinput,dinput);
        if(result==0){
          return 0;
        }
      }else if(strcmp(command, "print_all")==0){
        /*print_all*/
        printf("print_all: Insertion Order\n" );
        print_all(list);
      }else if(strcmp(command,"print_sort")==0){
        /*print_sort*/
        printf("print_sort: Key Sort Order\n" );
        print_sort(list);
      }else if(strcmp(command,"size")==0){
        /*size*/
        result=size(list);
        if(result==0){
          return 0;
        }
        printf("size:       List size=%d\n",list->size );
      }else if(strcmp(command, "empty")==0){
        /*empty list*/
        empty_list(list);
        size(list);
        printf("empty:       size:%d\n",list->size );
      }else if(strcmp(command,"rem_small")==0){
        /*remove smallest*/
        result=remove_smallest_key(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_small: %.2f  %d\n",*key, *value );
      }else if(strcmp(command,"rem_large")==0){
        /*removes largest*/
        result=remove_largest_key(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_large: %.2f  %d\n",*key,*value );
      }else if(strcmp(command,"rem_first")==0){
        /*remove first*/
        result=remove_first(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_first: %.2f  %d\n",*key,*value );
      }else if(strcmp(command,"rem_last")==0){
        /*rem last*/
        result=remove_last(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_last:  %.2f  %d\n",*key,*value );
      }

      dinput=0;
      iinput=0;
    }
  /*frees sutff*/
  destroy_list(list);
  free(command);
  free(key);
  free(value);
  free(input);
  if(fp!=NULL){
    fclose(fp);
  }
  return 1;
}
