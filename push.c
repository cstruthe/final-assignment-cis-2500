#include "header.h"
int push(Sorted_List *list, value_type value, key_type key ){
  Node* new_node = create_node();
  Node *node=list->head;
  /*checks to make sure node was made*/
  if(new_node==NULL){
    return 0;
  }

  /*sets next point of the list to the next head*/
  if(node!=NULL){
    new_node->next=node;
    list->head=new_node;
    node->prev=new_node;
  }
  /*sets stuff equal to list parts depending on int or char*/
  #ifdef INT
    new_node->key=key;
    new_node->value=value;
  #endif
  #ifdef CHAR
    strcpy(new_node->value,value);
    new_node->key=key;
  #endif
  #ifdef FRACT
  Fraction *ftemp=malloc(sizeof(Fraction));
  ftemp->num=value->num;
  ftemp->denom=value->denom;
  new_node->key=key;
  new_node->value=ftemp;
  #endif
  /*remakes the sort list*/
  if(list->head!=NULL){
      node=list->sort_head;
      node->prev=node;
      list->sort_head=sorting(list);
  node=list->sort_head;
  }else{
    /*if there's nothing in the list yet it'll set everything equal to first point*/
    list->head=new_node;
    list->sort_head=new_node;
    list->sort_tail=new_node;
    list->tail=new_node;
  }
  /*remakes the prev stuff*/
  prev(list);
  prev_sort(list);
  if(list->sort_head==NULL){
    return 0;
  }
 return 1;
}
