#include "header.h"

void prev(Sorted_List *list){
  int i=0;
  Node * node=list->tail;
  Node *temp=list->head;
  /*goes through the list to and find each other previous by checking if the next is equal to said point*/
  for(i=0; i<list->size; i++){
    temp=list->head;
    while(temp!=NULL&&temp->next!=node){
      temp=temp->next;
    }
    node->prev=temp;
    node=temp;
  }
  temp=list->head;
  temp->prev=NULL;
}
