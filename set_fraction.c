#include "header.h"

int set_fraction(Fraction *fract,long long num,long long denom){
  if(denom<0){
    /*if denom is negative*/
    fract->denom=-denom;
  }else if(denom==0){
    /*if denom is false*/
    return FALSE;
  }else{
    /*if nothing is wrong*/
    fract->denom=denom;
  }
    fract->num=num;
  return TRUE;
}
