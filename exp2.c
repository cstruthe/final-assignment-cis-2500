#include "header.h"

value_type exp2 (value_type value,value_type exponent){
  /*puts function to power of two*/
  value_type power=pow(value,exponent);
  return (int) power;
}
