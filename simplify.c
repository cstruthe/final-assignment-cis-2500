#include "header.h"

void simplify (Fraction *fract){
  /*find gcd then divides by that value*/
  long long g;
  g=gcd(fract->num,fract->denom);
  fract->num=fract->num/g;
  fract->denom=fract->denom/g;
}
