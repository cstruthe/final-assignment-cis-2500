#include "header.h"

void empty_list(Sorted_List *list){
  Node* node=list->head;
  /*goes node by node and frees them*/
  while(node!=NULL){

      free(node);
      node=node->next;
    }
  list->head=NULL;
  list->tail=NULL;
  list->sort_tail=NULL;
  list->sort_head=NULL;
}
