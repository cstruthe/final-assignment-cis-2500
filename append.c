#include "header.h"

int append(Sorted_List *list, value_type value, key_type key ){
  int i=0;
  Node*new=create_node();
  Node *temp=list->head;
  if(new==NULL){
    return 0;
  }
  #ifdef INT
  new->key=key;
  new->value=value;
  #endif
  #ifdef CHAR
  strcpy(new->value,value);
  new->key=key;
  #endif
  #ifdef FRACT
  Fraction *ftemp=malloc(sizeof(Fraction));
  ftemp->num=value->num;
  ftemp->denom=value->denom;
  new->key=key;
  new->value=ftemp;
  #endif
  if(temp!=NULL){
  temp=list->tail;
  temp->next=new;
  list->tail=new;
  temp=list->sort_head;
  i=0;
  for(i=0; i<size(list); i++){
    if(temp->key<key&& temp->sort!=NULL){

    temp=temp->sort;
    i++;
  }
  }
  if(i==0){
    list->sort_head=new;
  }else if(i==size(list)){
    list->sort_tail=new;
  }

  list->sort_head=sorting(list);
  prev(list);
  prev_sort(list);
}else{
  list->head=new;
  list->sort_head=new;
  list->sort_tail=new;
  list->tail=new;
}
  return 1;
}
