#include "header.h"

int remove_largest_key(Sorted_List *list, value_type* value, key_type *key){
  int i=0;
  int result=1;
  Node* temp;
  Node *temp2;
  Node *temp3;
  /*chekcs the head*/
  if(list->sort_head==NULL){
    return 0;
  }else{
    temp=list->sort_head;
  }
  /*copies the value*/
  #ifdef INT
  *key=temp->key;
  *value=temp->value;
  #endif
  #ifdef CHAR
  *key=temp->key;
  strcpy(*value,temp->value);
  #endif
  /*makes new head*/
  if(temp->sort!=NULL){
    temp2=temp->sort;
    list->sort_head=temp2;
  }else{
    list->sort_head=NULL;
    return result;
  }
  temp2=list->head;
  /*reagrannes original*/
  while(temp!=temp2&&temp2!=NULL){
    temp3=temp2;
    temp2=temp2->next;
  }
  /*checks if list head and list tail need to change*/
  if(temp==list->head){
    list->head=temp->next;
  }else{
    if(temp2->next!=NULL){
      temp3->next=temp2->next;
    }else{
      temp3->next=NULL;
      list->tail=temp3;
    }
  }

  temp2=list->head;
  list->size--;
  for(i=0; i<list->size; i++){
   temp2=temp2->next;
  }
  temp2=NULL;
  /*resorts prevs*/
  prev(list);
  prev_sort(list);
  free(temp);
  temp=NULL;
  return result;
}
