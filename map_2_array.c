#include "header.h"

 value_type *map_2_array(Sorted_List *list,Sorted_List *list2, value_type(*map_2_array)(value_type,value_type),int order){
   int i=0;
   /*checks size*/
   if(list->size!=list2->size){
     printf("error list are not the same size\n");
     return NULL;
   }
   value_type *result=malloc(sizeof(value_type)*list->size);
   Node *node;
   Node *node2;
   /*if Insertion order*/
   if(order==0){
     node=list->head;
     node2=list2->head;
     /*applys the function pointer*/
     for(i=0; i<list->size; i++){
       result[i]=map_2_array(node->value,node2->value);
       node=node->next;
       node2=node2->next;
     }
   }else if(order==1){
     /*if sorted order*/
     node=list->sort_tail;
     node2=list2->sort_tail;
     for(i=0; i<list->size; i++){
       /*applys the function pointer*/
       result[i]=map_2_array(node->value,node2->value);
       node=node->prev_sorted;
       node2=node2->prev_sorted;
     }
   }
   return result;
 }
