#include "header.h"

void remove_nth(Sorted_List *list ,Node *node,int count,int number,int order){
  Node *temp;
  if(order==0){
    if(count==number&&number!=0){
      /*INSERTED_ORDER*/
      node=list->head;
      count--;
      if(node->next!=NULL){
        remove_nth(list,node->next,count,number,order);
      }else {
        printf("remove_nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
      }
    }else if(count==0&&number!=0){
        printf("remove_nth:    n=%d, INSERTED_ORDER\n",number);
        printf("     %.2f %d\n",node->key, node->value );
        temp=node->prev;
        temp->next=node->next;
        if(node==list->tail){
          temp=list->tail;
          list->tail=temp->prev;
          temp=temp->prev;
          temp->next=NULL;
        }
        free(node);
        sorting(list);
        prev(list);
        prev_sort(list);
      }else if(number==0){
        node=list->head;
        printf("remove_nth:    n=%d, INSERTED_ORDER\n",number);
        printf("     %.2f %d\n",node->key, node->value );

        list->head=node->next;
        free(node);
        sorting(list);
        prev(list);
        prev_sort(list);
      }else{
        if(node->next!=NULL){
          count--;
          remove_nth(list,node->next,count,number,order);
        }else {

          printf("remove_nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
        }
      }
  }else if(order==1){
    if(count==number&&number!=0){
      node=list->sort_tail;
      count--;
      if(node->prev_sorted!=NULL){
        remove_nth(list,node->prev_sorted,count,number,order);
      }else {
        printf("remove_nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
      }
    }else if(count==0&&number!=0)
    {
    printf("remove_nth:    n=%d, SORTED_ORDER\n",number);
    printf("     %.2f %d\n",node->key, node->value );
    list->size--;
    temp=list->head;

    if(temp==node){
      if(temp->next!=NULL){
        list->head=temp->next;
        free(node);
        sorting(list);
        prev(list);
        prev_sort(list);
        return;
      }else{
        return;
      }
    }else if(node==list->tail){
      temp=list->tail;
      if(temp->prev!=NULL){
        list->tail=temp->prev;
        temp=temp->prev;
        temp->next=NULL;
        free(node);
        sorting(list);
        prev(list);
        prev_sort(list);
        return;
      }else{
        return;
      }
    }
    temp=list->head;
    while(temp->next!=node&&temp->next!=NULL){
      temp=temp->next;
    }
    if(node->next!=NULL){
      temp->next=node->next;
    }else{
      temp->next=NULL;
    }

    free(node);
    sorting(list);
    prev(list);
    prev_sort(list);

    }else if(number==0){
      node=list->sort_tail;
      printf("remove_nth:    n=%d, SORTED_ORDER\n",number);
      printf("     %.2f %d\n",node->key, node->value );
      list->size--;
      temp=list->head;

      if(temp==node){
        if(temp->next!=NULL){
          list->head=temp->next;
          free(node);
          sorting(list);
          prev(list);
          prev_sort(list);
          return;
        }else{
          return;
        }
      }else if(node==list->tail){
        temp=list->tail;
        if(temp->prev!=NULL){
          list->tail=temp->prev;
          temp=temp->prev;
          temp->next=NULL;
          free(node);
          sorting(list);
          prev(list);
          prev_sort(list);
          return;
        }else{
          return;
        }
      }
      temp=list->head;
      while(temp->next!=node&&temp->next!=NULL){
        temp=temp->next;
      }
      if(node->next!=NULL){
        temp->next=node->next;
      }else{
        temp->next=NULL;
      }

      free(node);
      sorting(list);
      prev(list);
      prev_sort(list);

    }else{
      if(node->prev_sorted!=NULL){
        count--;
        remove_nth(list,node->prev_sorted,count,number,order);
      }else {
        printf("remove_nth:    n=%d, FAILED, n>= size where size=%d\n",number,list->size );
      }
    }

  }

}
/**/
