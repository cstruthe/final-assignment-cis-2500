#include "header.h"

int run(char *command, key_type dinput,value_type iinput,Sorted_List *list1,Sorted_List *list2, char *command2,int listone,int listtwo,int order){
  Sorted_List *sqared=NULL;
  int i=0;
  value_type sum=0;
  value_type *sum_array;
  value_type result=0;
  value_type *value=malloc(sizeof(value_type));
  key_type *key=malloc(sizeof(key_type));
  /*push*/
  if(strcmp(command,"p")==0){
    printf("%s:            list=%2d      %.2f  %d\n",command,listone,dinput,iinput );
    result=push(list1,iinput,dinput);
    if(result==0){
      printf("error did not push properly\n");
      return 0;
    }
    /*append*/
  }else if(strcmp(command,"a")==0){
    printf("%s:            list=%2d      %.2f  %d\n",command,listone,dinput,iinput );
    result=append(list1,iinput,dinput);
    if(result==0){
      printf("error did not append properly\n");
      return 0;
    }
    /*print all*/
  }else if(strcmp(command, "print_all")==0){
    printf("print_all:    list=%2d Insertion Order\n",listone );
    print_all(list1);
  }else if(strcmp(command,"print_sort")==0){
    /*print sort*/
    printf("print_sort:    list=%2d Key Sort Order\n",listone );
    print_sort(list1);
  }else if(strcmp(command,"square")==0){
    /*square*/
    printf("%s:        list=%2d\n",command,listone );
    sqared=map(list1,exp2);
    print_all(sqared);
    destroy_list(sqared);
  }else if(strcmp(command,"sum")==0){
    /*sum*/
    sum=reduce(list1,add,0,0);
    printf("%s            list=%2d    result=%d\n",command,listone,sum);

  }else if(strcmp(command,"diff")==0){
    /*diff*/
    printf("diff:          list=%2d  list2=%2d",listone,listtwo );
    if(order==0){
      printf(" Insertion Sort Order\n" );
    }else if(order==1){
      printf(" Key Sort Order\n");
    }
    sum_array=map_2_array(list1,list2,subtract,order);
    if(sum_array==NULL){
      return 0;
    }
    for(i=0; i<list1->size; i++){
      if(sum_array[i]!=0){
        printf("%d\n",sum_array[i] );
      }
    }
    free(sum_array);
  }else if(strcmp(command,"sum_sq_d")==0){
    /*sum square diff*/
    result=map_2_reduce(list1,list2,exp2,subtract,order);
    printf("%s:      list=%2d   list2=%2d",command,listone,listtwo );
    if(order==0){
      printf(" Insertion Order");
    }else if(order==1){
      printf(" Key Sort Order ");
    }
    printf("  result=%d\n",result );
  }else if(strcmp(command,"rem_large")==0){
    /*remove large*/
    result=remove_largest_key(list1,value,key);
    if(result==0){
      return 0;
    }
    printf("rem_large:     list= %d   %.2f  %d\n",listone,*key,*value );
  }else if(strcmp(command,"rem_first")==0){
    /*remove first*/
    result=remove_first(list1,value,key);
    if(result==0){
      return 0;
    }
    printf("rem_first:     list= %d   %.2f  %d\n",listone,*key,*value );
  }else if(strcmp(command,"rem_last")==0){
    /*remove last*/
    result=remove_last(list1,value,key);
    if(result==0){
      return 0;
    }
    printf("rem_last:     list= %d   %.2f  %d\n",listone,*key,*value );
  }else if(strcmp(command,"rem_small")==0){
    result=remove_smallest_key(list1,value,key);
    if(result==0){
      return 0;
    }
    printf("rem_small:     list= %d   %.2f  %d\n",listone,*key,*value );
  }else if(strcmp(command,"size")==0){
    /*size*/
    result=size(list1);
    if(result==0){
      return 0;
    }
    printf("size:          list= %d   list size=%d\n",listone,list1->size );
  }else if(strcmp(command, "empty")==0){
    /*empty list*/
    empty_list(list1);
    size(list1);
    printf("empty:         list= %d   size:%d\n",listone,list1->size );
  }

  return 1;
}
