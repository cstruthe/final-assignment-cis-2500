#include "header.h"

value_type reduce (Sorted_List *list, value_type(*reduce)(value_type,value_type),value_type init,int order){
  int i=0;
  Node * node;
  value_type result=init;
  /*Inserted order*/
  if(order==0){
    node=list->head;
    for(i=0; i<size(list); i++){
      result=reduce(node->value,result);
      node=node->next;
    }
  }else if(order==1){
    /*sorted tail*/
    node=list->sort_tail;
    for(i=0; i<size(list); i++){
      result=reduce(node->value,result);
      node=node->prev_sorted;
    }
  }
  return result;
}
