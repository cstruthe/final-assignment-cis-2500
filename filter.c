#include "header.h"

Sorted_List *filter(Sorted_List *list, int(filter_ptr(Node*))){
  Node *node=list->head;
  Sorted_List *new_list=malloc(sizeof(Sorted_List));
  while(node!=NULL){
    /*copies the values into a new list if the filter_ptr is true*/
    if(filter_ptr(node)==TRUE){
      append(new_list,node->value,node->key);
    }
    node=node->next;
  }
  return new_list;
}
