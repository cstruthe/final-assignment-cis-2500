#include "header.h"
void print_sort(Sorted_List *list){
  Node *next_node=list->sort_tail;
  while(next_node!=NULL){
    /*for int*/
    #ifdef INT
    printf("     %.2f %d \n",next_node->key,next_node->value );
    #endif
    /*for char*/
    #ifdef CHAR
    printf("     %d   %s",next_node->key,next_node->value );
    #endif
    next_node=next_node->prev_sorted;
  }

}
