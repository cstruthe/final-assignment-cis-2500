#include "header.h"

void prev_sort(Sorted_List *list){
  int i=0;
  Node * node=list->sort_tail;
  Node *temp=list->sort_head;
  /*goes through the list to and find each other previous by checking if the next is equal to said point*/
  for(i=0; i<list->size; i++){
    temp=list->sort_head;
    while(temp!=NULL&&temp->sort!=node){
      temp=temp->sort;
    }
    node->prev_sorted=temp;
    node=temp;
  }
  temp=list->sort_head;
  temp->prev_sorted=NULL;
}
