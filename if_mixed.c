#include "header.h"

int if_mixed(Node *node){
  Fraction * temp=node->value;
  /*determine if number is mixed, is not returns TRUE*/
  if(temp->num<temp->denom||temp->num%temp->denom==0){
    return TRUE;
  }else{
    return FALSE;
  }
}
