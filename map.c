#include "header.h"

Sorted_List *map(Sorted_List *list, value_type (*fn_ptr)(value_type,value_type)){
  int i=0;
  Node *node=list->head;
  /*makes a new list and apply the function to the copied list.*/
  Sorted_List *new_list=malloc(sizeof(Sorted_List));
  for(i=0; i<list->size; i++){
  append(new_list,fn_ptr(node->value,2),node->key);
  node=node->next;
  }
  sorting(new_list);
  return new_list;
}
