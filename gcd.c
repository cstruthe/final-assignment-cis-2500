#include "header.h"

long long gcd(long long x, long long y){
  long long results;
  long long b;
  long long a;
  long long temp;
  a=x;
  b=y;
  /*uses Euclidean therom to find gcd*/
  /* code snippet taken from https://www.programmingsimplified.com/c/source-code/c-program-find-hcf-and-lcm*/
  /*find the remainder then replaces the diving with remainder*/
  while(b!=0){
    temp=b;
    b=a%b;
    a=temp;
  }
  results=a;
  return results;
}
