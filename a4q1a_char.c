#include "header.h"

int main(int argc,char *argv[]){
  FILE *fp;
  value_type *value=malloc(sizeof(value_type));
  char *command=malloc(sizeof(char)*80);
  value_type input;
  key_type iinput=0;
  key_type *key=malloc(sizeof(key_type));
  Sorted_List *list=malloc(sizeof(Sorted_List));
  list->size=0;
  int result=0;
  int loop=0;
  int i=0;
  /*checks argv*/
  if(argv[1]!=NULL){
  fp=fopen(argv[1], "r");
  }
  if(fp==NULL){
    printf("please enter a file name\n");
    do{
        fgets(input,80,stdin);
        if(feof(stdin)!=0){
          loop=1;
        }
      }while(loop==0);
    fp=fopen(input,"r");
  }
  if(fp==NULL){
    printf("error file not found\n");
    return 0;
  }

  memset(input,0,80);
  /*gets commane*/
    while(fscanf(fp,"%s",command)!=EOF){
      /*gets string*/
      fgets(input, 80,fp);
      while(input[0]==' '){
        for(i=0; i<strlen(input); i++){
          input[i]=input[i+1];
        }
      }
      iinput=strlen(input)-1;

      if(strcmp(command,"p")==0){
        /*push*/
        printf("%s:         %d  %s",command,iinput,input );
        result=push(list,input,iinput);
        if(result==0){
          return 0;
        }
      }else if(strcmp(command,"a")==0){
        /*append*/
        printf("%s:         %d  %s",command,iinput,input );
        result=append(list,input,iinput);
        if(result==0){
          return 0;
        }
      }else if(strcmp(command, "print_all")==0){
        /*print_all*/
        printf("print_all: Insertion Order\n" );
        print_all(list);
      }else if(strcmp(command,"print_sort")==0){
        printf("print_sort: Key Sort Order\n" );
        print_sort(list);
      }else if(strcmp(command,"size")==0){
        /*size*/
        result=size(list);
        if(result==0){
          return 0;
        }
        printf("size:       List size=%d\n",list->size );
      }else if(strcmp(command, "empty")==0){
        /*empty list*/
        empty_list(list);
        size(list);
        printf("empty:       size:%d\n",list->size );
      }else if(strcmp(command,"rem_small")==0){
        result=remove_smallest_key(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_small: %d  %s",*key, *value );
      }else if(strcmp(command,"rem_large")==0){
        /*remove large*/
        result=remove_largest_key(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_large: %d  %s",*key,*value );
      }else if(strcmp(command,"rem_first")==0){
        /*remove first*/
        result=remove_first(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_first: %d  %s",*key,*value );
      }else if(strcmp(command,"rem_last")==0){
        /*remove last*/
        result=remove_last(list,value,key);
        if(result==0){
          return 0;
        }
        printf("rem_last:  %d  %s",*key,*value );
      }
      iinput=0;
      memset(input,0,80);
    }
  /*frees stuff*/
  destroy_list(list);
  free(command);
  free(key);
  free(value);
  if(fp!=NULL){
    fclose(fp);
  }
  return 1;
  return 1;
}
