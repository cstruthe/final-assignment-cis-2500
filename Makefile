clean_all:clean_1 clean_2 clean_3 clean_4 clean_5

a4q1a_int:a4q1a_int.o size.o destroy_list.o create_node.o push.o sorting.o print_sort.o  append.o remove_first.o remove_last.o remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o
	gcc -ansi -Wall -DINT  -o a4q1a_int a4q1a_int.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o  remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o
a4q1a_int.o:a4q1a_int.c
	gcc -ansi -Wall -DINT -c a4q1a_int.c
size.o:size.c
	gcc -ansi -Wall -DINT -c size.c
create_node.o:create_node.c
	gcc -ansi -Wall -DINT -c create_node.c
push.o:push.c
	gcc -ansi -Wall -DINT -c push.c
sorting.o:sorting.c
	gcc -ansi -Wall -DINT -c sorting.c
clean_1:
	rm a4q1a_int a4q1a_int.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o  remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o
destroy_list.o:destroy_list.c
	gcc -ansi -Wall -DINT -c destroy_list.c
print_sort.o:print_sort.c
	gcc -ansi -Wall -DINT -c print_sort.c
remove_first.o:remove_first.c
	gcc -ansi -Wall -DINT -c remove_first.c
append.o:append.c
	gcc -ansi -Wall -DINT -c append.c
remove_last.o:remove_last.c
	gcc -ansi -Wall -DINT -c remove_last.c
remove_smallest_key.o:remove_smallest_key.c
	gcc -ansi -Wall -DINT -c remove_smallest_key.c
remove_largest_key.o:remove_largest_key.c
	gcc -ansi -Wall -DINT -c remove_largest_key.c
empty_list.o:empty_list.c
	gcc -ansi -Wall -DINT -c empty_list.c
print_all.o:print_all.c
	gcc -ansi -Wall -DINT -c print_all.c
prev.o:prev.c
	gcc -ansi -Wall -DINT -c prev.c
prev_sort.o:prev_sort.c
	gcc -ansi -Wall -DINT -c prev_sort.c




a4q1a_char:a4q1a_char.o sizec.o destroy_listc.o create_nodec.o pushc.o sortingc.o print_sortc.o  appendc.o remove_firstc.o remove_lastc.o  remove_smallest_keyc.o remove_largest_keyc.o empty_listc.o print_allc.o prevc.o prev_sortc.o
	gcc -ansi -Wall -DCHAR  -o a4q1a_char a4q1a_char.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o
a4q1a_char.o:a4q1a_char.c
	gcc -ansi -Wall -DCHAR  -c a4q1a_char.c
sizec.o:size.c
	gcc -ansi -Wall -DCHAR -c size.c
create_nodec.o:create_node.c
	gcc -ansi -Wall -DCHAR  -c create_node.c
pushc.o:push.c
	gcc -ansi -Wall -DCHAR -c push.c
sortingc.o:sorting.c
	gcc -ansi -Wall -DCHAR  -c sorting.c
clean_2:
	rm a4q1a_char a4q1a_char.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o
destroy_listc.o:destroy_list.c
	gcc -ansi -Wall -DCHAR -c destroy_list.c
print_sortc.o:print_sort.c
	gcc -ansi -Wall -DCHAR -c print_sort.c
remove_firstc.o:remove_first.c
	gcc -ansi -Wall -DCHAR -c remove_first.c
appendc.o:append.c
	gcc -ansi -Wall -DCHAR -c append.c
remove_lastc.o:remove_last.c
	gcc -ansi -Wall -DCHAR -c remove_last.c

remove_smallest_keyc.o:remove_smallest_key.c
	gcc -ansi -Wall -DCHAR -c remove_smallest_key.c
remove_largest_keyc.o:remove_largest_key.c
	gcc -ansi -Wall -DCHAR -c remove_largest_key.c
empty_listc.o:empty_list.c
	gcc -ansi -Wall -DCHAR -c empty_list.c
print_allc.o:print_all.c
	gcc -ansi -Wall -DCHAR -c print_all.c
prevc.o:prev.c
	gcc -ansi -Wall -DCHAR -c prev.c
prev_sortc.o:prev_sort.c
	gcc -ansi -Wall -DCHAR -c prev_sort.c


a4q1b:a4q1b.o map.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o  remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o exp2.o reduce.o add.o mult.o map_2_array.o map_reduce.o map_2_reduce.o run.o subtract.o
	gcc -ansi -Wall -lm -DINT -o a4q1b a4q1b.o map.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o  remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o exp2.o reduce.o add.o mult.o map_2_array.o map_reduce.o map_2_reduce.o run.o subtract.o
a4q1b.o:a4q1b.c
	gcc -ansi -Wall -DINT -c a4q1b.c
map.o:map.c
	gcc -ansi -Wall -lm -DINT -c map.c
exp2.o:exp2.c
	gcc -ansi -Wall -DINT -c exp2.c
reduce.o:reduce.c
	gcc -ansi -Wall -DINT -c reduce.c
add.o:add.c
	gcc -ansi -Wall -DINT -c add.c
mult.o:mult.c
	gcc -ansi -Wall -DINT -c mult.c
map_2_array.o:map_2_array.c
	gcc -ansi -Wall -DINT -c map_2_array.c
map_reduce.o:map_reduce.c
	gcc -ansi -Wall -DINT -c map_reduce.c
map_2_reduce.o:map_2_reduce.c
	gcc -ansi -Wall -DINT -c map_2_reduce.c
run.o:run.c
	gcc -ansi -Wall -DINT -c run.c
subtract.o:subtract.c
	gcc -ansi -Wall -DINT -c subtract.c

clean_3:
	rm a4q1b a4q1b.o map.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o  remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o exp2.o reduce.o add.o mult.o map_2_array.o map_reduce.o map_2_reduce.o run.o subtract.o



a4q2:a4q2.o size.o destroy_list.o create_node.o push.o sorting.o print_sort.o  append.o remove_first.o remove_last.o remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o count_up.o count_down.o nth.o remove_nth.o
	gcc -ansi -Wall -DINT  -o a4q2 a4q2.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o  remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o count_up.o count_down.o nth.o remove_nth.o
clean_4: 	
	rm a4q2 a4q2.o size.o destroy_list.o create_node.o  push.o sorting.o print_sort.o append.o remove_first.o remove_last.o  remove_smallest_key.o remove_largest_key.o empty_list.o print_all.o prev.o prev_sort.o count_up.o count_down.o nth.o remove_nth.o


a4q2.o:a4q2.c
	gcc -ansi -Wall -DINT -c a4q2.c
count_up.o:count_up.c
	gcc -ansi -Wall -DINT -c count_up.c 
count_down.o:count_down.c
	gcc -ansi -Wall -DINT -c count_down.c
nth.o:nth.c
	gcc -ansi -Wall -DINT -c nth.c
remove_nth.o:remove_nth.c
	gcc -ansi -Wall -DINT -c remove_nth.c



a4q3:a4q3.o set_fraction.o print_fract.o simplify.o gcd.o add_fract.o filter.o append_f.o size_f.o sorting_f.o prev_f.o prev_sort_f.o create_node_f.o push_f.o print_all_fract.o print_sort_fract.o if_fract.o destroy_list_f.o if_whole.o if_mixed.o
	gcc -ansi -Wall -DFRACT -o a4q3 a4q3.o set_fraction.o print_fract.o simplify.o gcd.o add_fract.o filter.o append.o size.o sorting.o prev.o prev_sort.o create_node.o push.o print_all_fract.o print_sort_fract.o if_fract.o destroy_list.o if_whole.o if_mixed.o
a4q3.o:a4q3.c
	gcc -ansi -Wall -DFRACT -c a4q3.c
set_fraction.o:set_fraction.c
	gcc -ansi -Wall -DFRACT -c set_fraction.c
print_fract.o:print_fract.c
	gcc -ansi -Wall -DFRACT -c print_fract.c
simplify.o:simplify.c
	gcc -ansi -Wall -DFRACT -c simplify.c
gcd.o:gcd.c
	gcc -ansi -Wall -DFRACT -c gcd.c
add_fract.o:add_fract.c
	gcc -ansi -Wall -DFRACT -c add_fract.c
append_f.o:append.c
	gcc -ansi -Wall -DFRACT -c append.c
filter.o:filter.c
	gcc -ansi -Wall -DFRACT -c filter.c
size_f.o:size.c
	gcc -ansi -Wall -DFRACT -c size.c
sorting_f.o:sorting.c
	gcc -ansi -Wall -DFRACT -c sorting.c
prev_f.o:prev.c
	gcc -ansi -Wall -DFRACT -c prev.c
prev_sort_f.o:prev_sort.c
	gcc -ansi -Wall -DFRACT -c prev_sort.c
create_node_f.o:create_node.c
	gcc -ansi -Wall -DFRACT -c create_node.c
push_f.o:push.c
	gcc -ansi -Wall -DFRACT -c push.c
print_all_fract.o:print_all_fract.c
	gcc -ansi -Wall -DFRACT -c print_all_fract.c
print_sort_fract.o:print_sort_fract.c
	gcc -ansi -Wall -DFRACT -c print_sort_fract.c
if_fract.o:if_fract.c
	gcc -ansi -Wall -DFRACT -c if_fract.c
destroy_list_f.o:destroy_list.c
	gcc -ansi -Wall -DFRACT -c destroy_list.c
if_whole.o:if_whole.c
	gcc -ansi -Wall -DFRACT -c if_whole.c
if_mixed.o:if_mixed.c
	gcc -ansi -Wall -DFRACT -c if_mixed.c
clean_5:
		rm a4q3 a4q3.o set_fraction.o print_fract.o simplify.o gcd.o add_fract.o filter.o append.o size.o sorting.o prev.o prev_sort.o create_node.o push.o print_all_fract.o print_sort_fract.o if_fract.o destroy_list.o if_whole.o if_mixed.o


